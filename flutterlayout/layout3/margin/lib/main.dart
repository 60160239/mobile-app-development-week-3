import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Center(
        child: Container(
          color: Color.fromARGB(255, 66, 166, 233),
          alignment: AlignmentDirectional(0,0),
          child: Container(
            child: Text('Hello Margin'),
            color: Colors.green,
            margin: EdgeInsets.only(left: 20.0, bottom: 40.0, top: 50.0),
            padding: EdgeInsets.all(40.0),
            transform: Matrix4.rotationZ(0.4),
          ),
        ),
      ),
    )
  );
}
